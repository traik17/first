from os import getenv
import allure
import pytest

@allure.epic('prod alpha')
@allure.story('ST-4')
@allure.feature('component-5')
@allure.suite('unittests')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_five():
    """
    Case: test five component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert True

@allure.epic('prod alpha')
@allure.story('ST-4')
@allure.feature('component-5')
@allure.suite('integration')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_six():
    """
    Case: test six component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert False
