from os import getenv
import allure
import pytest

@allure.epic('prod alpha')
@allure.story('ST-2')
@allure.feature('component-1')
@allure.suite('unittests')
@allure.severity(allure.severity_level.CRITICAL)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_three():
    """
    Case: test third component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert True

@allure.epic('prod alpha')
@allure.story('ST-3')
@allure.feature('component-3')
@allure.suite('unittests')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_four():
    """
    Case: test four component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert False
