from os import getenv
import allure
import pytest

@allure.epic('prod alpha')
@allure.story('ST-1')
@allure.feature('component-1')
@allure.suite('unittests')
@allure.severity(allure.severity_level.BLOCKER)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_one():
    """
    Case: test first component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert True

@allure.epic('prod alpha')
@allure.story('ST-1')
@allure.feature('component-2')
@allure.suite('unittests')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.skipif(getenv('SKIP_TESTS', None), reason="Runned only for collecting tests")
def test_two():
    """
    Case: test second component
    Steps:
    - run 
    - pass
    Expected: all ok
    """
    assert True
